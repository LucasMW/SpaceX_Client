/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation


struct CompanyInfo : Codable {
	let headquarters : Headquarters?
	let links : Links?
	let name : String?
	let founder : String?
	let founded : Int?
	let employees : Int?
	let vehicles : Int?
	let launch_sites : Int?
	let test_sites : Int?
	let ceo : String?
	let cto : String?
	let coo : String?
	let cto_propulsion : String?
	let valuation : Int?
	let summary : String?
	let id : String?

	enum CodingKeys: String, CodingKey {

		case headquarters = "headquarters"
		case links = "links"
		case name = "name"
		case founder = "founder"
		case founded = "founded"
		case employees = "employees"
		case vehicles = "vehicles"
		case launch_sites = "launch_sites"
		case test_sites = "test_sites"
		case ceo = "ceo"
		case cto = "cto"
		case coo = "coo"
		case cto_propulsion = "cto_propulsion"
		case valuation = "valuation"
		case summary = "summary"
		case id = "id"
	}
    
    static func  getDefault() -> CompanyInfo {
        let json = """
{
    "headquarters": {
        "address": "Rocket Road",
        "city": "Hawthorne",
        "state": "California"
    },
    "links": {
        "website": "https://www.spacex.com/",
        "flickr": "https://www.flickr.com/photos/spacex/",
        "twitter": "https://twitter.com/SpaceX",
        "elon_twitter": "https://twitter.com/elonmusk"
    },
    "name": "SpaceX",
    "founder": "Elon Musk",
    "founded": 2002,
    "employees": 9500,
    "vehicles": 4,
    "launch_sites": 3,
    "test_sites": 3,
    "ceo": "Elon Musk",
    "cto": "Elon Musk",
    "coo": "Gwynne Shotwell",
    "cto_propulsion": "Tom Mueller",
    "valuation": 74000000000,
    "summary": "SpaceX designs, manufactures and launches advanced rockets and spacecraft. The company was founded in 2002 to revolutionize space technology, with the ultimate goal of enabling people to live on other planets.",
    "id": "5eb75edc42fea42237d7f3ed"
}
"""
        let jsonDecoder = JSONDecoder()
        let data = json.data(using: .utf8)
        let responseModel = try! jsonDecoder.decode(CompanyInfo.self, from: data!)
        return responseModel

    }

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		headquarters = try values.decodeIfPresent(Headquarters.self, forKey: .headquarters)
		links = try values.decodeIfPresent(Links.self, forKey: .links)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		founder = try values.decodeIfPresent(String.self, forKey: .founder)
		founded = try values.decodeIfPresent(Int.self, forKey: .founded)
		employees = try values.decodeIfPresent(Int.self, forKey: .employees)
		vehicles = try values.decodeIfPresent(Int.self, forKey: .vehicles)
		launch_sites = try values.decodeIfPresent(Int.self, forKey: .launch_sites)
		test_sites = try values.decodeIfPresent(Int.self, forKey: .test_sites)
		ceo = try values.decodeIfPresent(String.self, forKey: .ceo)
		cto = try values.decodeIfPresent(String.self, forKey: .cto)
		coo = try values.decodeIfPresent(String.self, forKey: .coo)
		cto_propulsion = try values.decodeIfPresent(String.self, forKey: .cto_propulsion)
		valuation = try values.decodeIfPresent(Int.self, forKey: .valuation)
		summary = try values.decodeIfPresent(String.self, forKey: .summary)
		id = try values.decodeIfPresent(String.self, forKey: .id)
	}
    
}
