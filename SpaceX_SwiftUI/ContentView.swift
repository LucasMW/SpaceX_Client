//
//  ContentView.swift
//  SpaceX_SwiftUI
//
//  Created by Lucas Menezes on 10/15/21.
//

import SwiftUI

extension Date {

    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {

        let currentCalendar = Calendar.current

        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }

        return end - start
    }
}
struct MissionCell : View {
    let launch : Launch
    
    var missionName : String {
        return launch.name ?? "Unamed"
    }
    var date : String {
        return launch.date_local != nil ? "\(launch.date_local!)" : "02:00"
    }
    var rocketName : String {
        return launch.launchpad ?? "Unnamed"
    }
    var days : String {
        //2008-09-28T23:15:00.000Z
        let dateFormatter = DateFormatter()
        print(launch.date_utc!)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        let date = dateFormatter.date(from: launch.date_utc!) ??
        
        Date.init(timeIntervalSinceReferenceDate: 0)
        let today = Date.init(timeIntervalSinceNow: 0)
        let diff = today.interval(ofComponent: .day, fromDate: date)
         
        
        return "\(diff) days"
    }
    var imageURL : URL {
        let defaultURL = URL(string: "http://104.131.251.97/soyuztma18m/wp-content/uploads/2015/09/Soyuz_TMA-1r8M_liftoff.jpg")!
        guard let patch = launch.links?.patch else {
            return defaultURL
        }
        return URL(string: patch.small ?? "") ?? defaultURL
    }
    
    
    
    var body: some View {
        ZStack {
            HStack {
                ///padding()
                RemoteImageView(
                    url: imageURL,
                    placeholder: {
                        Image.init(systemName: "checkmark").frame(width: 30) // etc.
                    },
                    image: {
                       // $0.scaledToFit().clipShape(Circle())// etc.
                        $0.scaleEffect(CGSize(width: 0.3, height:    0.3)).padding()
                    }
                ).frame(width: 3, height: 3)
                Spacer()
                Image.init(systemName: "checkmark").frame(width: 100, height: 100)
            }.frame(height: 100)//.background(Color.yellow)
            VStack {
                HStack {
                    
                    Spacer()
                    Text(missionName)
                    Spacer()
                    //Image.init(systemName: "checkmark")
                    
                }
                HStack {
                    
                    Spacer()
                    Text(date)
                    Spacer()
                    ///Image.init(systemName: "checkmark")
                    
                }
                HStack {
                  //  Image.init(systemName: "magnifyingglass.circle.fill")
                    Spacer()
                    Text(rocketName)
                    Spacer()
                    //Image.init(systemName: "checkmark")
                    
                }
                HStack {
                    ///Image.init(systemName: "magnifyingglass.circle.fill")
                    Spacer()
                    Text(days)
                    Spacer()
                   // Image.init(systemName: "checkmark")
                    
                }
            }
        }
        
    }
}

class SpaceXViewModel : ObservableObject {
    @Published var launchList = [Launch]()
    @Published var companyInfo : CompanyInfo?
    
    func load() {
        SpaceXAPI().getAllLaunches { launches, error in
            print("Loaded")
            if error == nil {
                print("\(launches.count)")
                self.launchList = launches
                print("\(launches.count)")
            } else {
                print("Deu ruim \(error)")
                
            }
        }
    }
}

struct ContentView: View {
    let companyInfo = CompanyInfo.getDefault()
    @ObservedObject var viewModel = SpaceXViewModel()
    
    func getString() -> String {
        return "\(companyInfo.name!) was founded by \(companyInfo.founder!) in \(companyInfo.founded!). It has now \(companyInfo.employees!) employees, \(companyInfo.launch_sites!) launch sites, and is valued at USD \(companyInfo.valuation!)"
    }
    var body: some View {
        VStack {
            HStack {
                Spacer()
                Text("Space X")
                    .padding()
                Spacer()
                Image.init(systemName: "magnifyingglass.circle.fill")
            }
            HStack {
                Spacer()
                
                Text("COMPANY")
                    .padding()
                    .frame(height: 23.0).colorInvert()
                
                Spacer()
                
            }.scaledToFill().background(Color.black)
            
            VStack {
                Text(
                    getString()
                )
            }
            HStack {
                Spacer()
                Text("LAUNCHES").colorInvert()
                Spacer()
            }.background(Color.black)
            List(viewModel.launchList){launch in
                //Text("x")
                MissionCell(launch: launch)
            }.onAppear {
                print("call loading")
                viewModel.load()
            }
            
            Spacer()
            
            
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
