//
//  SpaceX_SwiftUIApp.swift
//  SpaceX_SwiftUI
//
//  Created by Lucas Menezes on 10/15/21.
//

import SwiftUI

@main
struct SpaceX_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
