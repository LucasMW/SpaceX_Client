//
//  SpaceX_V3.swift
//  SpaceX_SwiftUI
//
//  Created by Lucas Menezes on 10/15/21.
//

import Foundation

class SpaceXAPI {
    let base = "https://api.spacexdata.com/v4"
    
    func getCompanyInfo(completion: @escaping (CompanyInfo?,Error?) -> ()) {
        let urlString = "https://api.spacexdata.com/v4/company"
        let url = URL(string: urlString)
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            guard let data = data else {
                return
            }
            do {
                let jsonDecoder = JSONDecoder()
                let responseModel = try jsonDecoder.decode(CompanyInfo.self, from: data)
                completion(responseModel,nil)
            }
            catch  {
                completion(nil,error )
            }
            
            
        }
        task.resume()
        
    }
    func getAllLaunches(completion: @escaping ([Launch],Error?) -> ()) {
        let urlString = "https://api.spacexdata.com/v4/launches"
        let url = URL(string: urlString)
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            guard let data = data else {
                return
            }
            do {
                let jsonDecoder = JSONDecoder()
                //print(String(data: data, encoding: .utf8) ?? "xxx")
                let responseModel = try jsonDecoder.decode([Launch].self, from: data)
                DispatchQueue.main.async {
                    completion(responseModel,nil)
                }
                
            }
            catch  {
                completion([],error )
            }
            
            
        }
        task.resume()
    }
    func getImageDataFromURL(urlString: String,  completion: @escaping (Data?,Error?) -> ()) {
        let url = URL(string: urlString)
        DispatchQueue.global().async {
            guard let data = try? Data(contentsOf: url!) else {
                completion(nil, NSError() )
                return
            }
            DispatchQueue.main.async {
                completion(data, nil)
            }
        }
    }
    
}
